# Contributor: Daniel Sabogal <dsabogalcc@gmail.com>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=msgpack-cxx
pkgver=4.1.0
pkgrel=0
pkgdesc="An efficient object serialization library for C++"
url="https://msgpack.org"
arch="all"
license="BSL-1.0"
depends_dev="boost-dev"
makedepends="$depends_dev cmake samurai"
checkdepends="gtest-dev zlib-dev"
subpackages="$pkgname-dev"
source="https://github.com/msgpack/msgpack-c/releases/download/cpp-$pkgver/msgpack-cxx-$pkgver.tar.gz"

build() {
	cmake -G Ninja -B build . \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_VERBOSE_MAKEFILE=ON \
		-DMSGPACK_BUILD_EXAMPLES=OFF \
		-DMSGPACK_BUILD_TESTS=ON \
		-DMSGPACK_CXX17=ON
	cmake --build build
}

check() {
	cmake --build build --target test
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
2bf4d27b3afd2beb782e64745ecf639878f4424a082001cbeef09c341282cea5acf6805d2307cf745cfeebfb4596b02072abaac8318f223366915b8578310a26  msgpack-cxx-4.1.0.tar.gz
"
